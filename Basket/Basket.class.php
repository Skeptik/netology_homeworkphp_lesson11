<?php
namespace Basket;
use Exception;
class Basket
{
    public $arrProduct = array();

    public function checkException($product)
    {
        if(!$product->getPrice()) {
            throw new Exception("Товар без цены нельзя добавить в корзину");
        }
        $this->arrProduct [] = $product;
    }

    public function addProduct($product)
    {
        try {
            $this->checkException($product);
        } catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }

    }
    public function delProduct($product)
    {

        foreach ($this->arrProduct as $key => $value) {
            if($value == $product) {
                unset($this->arrProduct[$key]);
            }
        }
    }
    public function summProduct()
    {
        $result=0;
        foreach ($this->arrProduct as $key => $value) {
            $result += $value->getPrice();
        }
        echo "<strong>Ваш заказ на сумму $result</strong><br>";
    }
}
?>