<?php
namespace DairyProduct;
require_once(__DIR__.'/../Product/Product.class.php');
class DairyProduct extends \Product\Product
{
	private $weight;

	public function setName($name)
	{
		$this->name=$name;
	}
	public function getName()
	{
		return $this->name;
	}

	public function setPrice($price)
	{
		$this->price=$price;
	}
	public function getPrice()
	{
		return $this->price;
	}
	public function setCategory($category)
	{
		$this->category;
	}
	public function getCategory()
	{
		return $this->category;
	}
	public function setWeight($weight)
	{
		$this->weight = $weight;
	}
	public function getWeight($weight)
	{
		return $this->weight;
	}
}
?>