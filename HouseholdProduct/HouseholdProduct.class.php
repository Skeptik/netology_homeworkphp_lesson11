<?php
namespace HouseholdProduct;
class HouseholdProduct extends \Product\Product
{
    private $discount;
    private $discPrice;

    public function setName($name)
    {
        $this->name=$name;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setPrice($price)
    {
        $this->price=$price;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function setCategory($category)
    {
        $this->category;
    }
    public function getCategory()
    {
        return $this->category;
    }
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    public function getWeight($weight)
    {
        return $this->weight;
    }
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }
    public function discountPrice()
    {
        if($this->discount) {
            $this->discPrice = round($this->price-($this->price * $this->discount / 100), 2);
            return $this->discPrice;
        } else {
            return $this->price;
        }
    }
}
?>