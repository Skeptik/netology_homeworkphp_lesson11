<?php
namespace Product;
abstract class Product
{
	protected $name;
	protected $price;
	protected $category;
	public function __construct($name, $price, $category)
	{
		$this->name=$name;
		$this->price=$price;
		$this->category=$category;
	}
	abstract public function setName($name);
	abstract public function getName();
	abstract public function setPrice($price);
	abstract public function getPrice();
	abstract public function setCategory($category);
	abstract public function getCategory();
}
?>