<!DOCTYPE html>
<html>
<head>
  <title>Домашнее задание: Классы и объекты</title>
  <style type="text/css">
    div {
   	  padding: 7px;
   	  padding-right: 20px;
      border: solid 1px black;
   	  font-family: Verdana, Arial, Helvetica, sans-serif;
   	  font-size: 13pt;
   	  background: #E6E6FA;
   	}
   	body {
   	  background: #159445;
   	}
  </style>
</head>
<body>
  <div>
    <center><h1>Пространство имен<h1></center>
	<p>
	  У нас не может быть два класса, названных одинаково, все они должны быть уникальны. Пространства имен позволяют нам обойти эту проблему, и мы можем создать столько классов с одинаковыми именами, сколько нам понадобится. Это некое декларированное хранилище, позволяющее группировать логически связанные классы, интерфейсы, функции и константы, чтоб избежать конфиликтов одинаковых имен.
	</p>
  </div>

  <div>
    <center><h1>Исключение<h1></center>
    <p>
      <strong>Exception</strong> - Это базовый класс для всех пользовательских исключений. Позволяет правильно реагировать на ошибки, возникающие в ходе выполнения программы. Используя механизм исключительных ситуаций, программа может автоматически вызывать процедуру обработки ошибок, на уровне языка и логики, принимать решения, что делать в случае ошибки, а не просто ее игнорировать.
    </p>
  </div>
</body>
</html>

<?php
echo '<center><h1>Товары</h1></center>';
function myAutoload($className) {
    $filePath = __DIR__.'/'
		    .str_replace('\\', '/', $className)
		    .'.class.php';
	  if (file_exists($filePath)){
		    require_once($filePath);
	  } else {
		    echo "Error file";
	  }
}

spl_autoload_register('myAutoload');

$cheese = new \DairyProduct\DairyProduct('Пармезан', 0, 'Молоный продукт');
$cottageCheese = new \DairyProduct\DairyProduct('Творог', 200, 'Молоный продукт');
$sponge = new \HouseholdProduct\HouseholdProduct('Губка', 40, 'Хоз. товар');
$fairy = new \HouseholdProduct\HouseholdProduct('Моющее Fairy', 150, 'Хоз. товар');

$basket = new \Basket\Basket();

$basket->addProduct($sponge);
$basket->addProduct($fairy);
echo '<br>';
$order = new \Order\Order($basket);
$basket->summProduct();

$basket->delProduct($sponge);
echo '<br>';
$order = new \Order\Order($basket);
$basket->summProduct();
echo '<br>';

$basket_my = new \Basket\Basket();
$basket_my->addProduct($cheese);
$basket_my->addProduct($sponge);
$basket_my->addProduct($cottageCheese);
$basket_my->addProduct($fairy);

echo '<br>';
$order_my = new \Order\Order($basket_my);
$basket_my->summProduct();
?>